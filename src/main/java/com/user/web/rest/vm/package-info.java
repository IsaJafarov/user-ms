/**
 * View Models used by Spring MVC REST controllers.
 */
package com.user.web.rest.vm;
